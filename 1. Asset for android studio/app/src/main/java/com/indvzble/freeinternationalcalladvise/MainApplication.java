package com.indvzble.freeinternationalcalladvise;

import android.app.Application;
import android.support.multidex.MultiDex;

import com.indvzble.freeinternationalcalladvise.manager.BusManager;
import com.indvzble.freeinternationalcalladvise.manager.DataManager;

/**
 * Created by willingdev on 5/17/15.
 */
public class MainApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(getApplicationContext());
        DataManager.getInstance().init(getApplicationContext());
        BusManager.initInstance();
    }
}
