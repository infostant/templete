package com.indvzble.freeinternationalcalladvise;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indvzble.freeinternationalcalladvise.util.RoundedImage;
import com.squareup.picasso.Picasso;

public class ItemDataAdapter extends ArrayAdapter<ItemData> {

	Context context;
	int layoutResourceId;
	ItemData data[] = null;
	private RoundedImage transformation, roundImage;

	public ItemDataAdapter(Context context, int layoutResourceId,
			ItemData[] data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
		transformation = new RoundedImage((int) RoundedImage.convertDpToPixel(
				8, context), RoundedImage.Corners.ALL);
		roundImage = new RoundedImage((int) RoundedImage.convertDpToPixel(80,
				context), RoundedImage.Corners.ALL);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		WeatherHolder holder = null;

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new WeatherHolder();

			// item_list_custom_1
//			holder.imgIcon = (ImageView) row.findViewById(R.id.imgMyIco);
//			holder.txtTitle = (TextView) row.findViewById(R.id.txtMyText);
//			holder.wrpFilter = (LinearLayout) row.findViewById(R.id.wrpFilter);
//			row.findViewById(R.id.wrpFilter);

			// item_list_custom_2
			 holder.imgIcon = (ImageView) row.findViewById(R.id.imgMyIcoA);
			 holder.txtTitle = (TextView) row.findViewById(R.id.txtMyTextA);
			 holder.wrpFilter = (LinearLayout)
			 row.findViewById(R.id.wrpFilterA);

			row.setTag(holder);
		} else {
			holder = (WeatherHolder) row.getTag();
		}

		ItemData weather = data[position];
		holder.txtTitle.setText(weather.title);

		Picasso.with(context)
				 .load("file:///android_asset/landingPage/img/" +
				 weather.icon).fit().centerCrop()
				.placeholder(R.drawable.ico_placeholder).into(holder.imgIcon);
		holder.wrpFilter.setBackgroundColor(Color
				.parseColor(weather.filterColor));

		Picasso.with(context)
				.load("file:///android_asset/landingPage/img/banner.jpg")
				.into((ImageView) ((Activity) context)
						.findViewById(R.id.bannerApp));

		if (weather.haveIcon == false) {
			holder.imgIcon.setVisibility(View.GONE);
		} else {
			holder.imgIcon.setVisibility(View.VISIBLE);
		}

		return row;
	}

	static class WeatherHolder {
		ImageView imgIcon;
		TextView txtTitle;
		LinearLayout wrpFilter;
	}
}