package com.indvzble.freeinternationalcalladvise.manager;

import android.content.Context;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

/**
 * Created by willingdev on 5/21/15.
 */
public class AdsManager {
    public static void crateAdsView(Context context,FrameLayout container) {
        AdView mAdView =  new AdView(context);
        mAdView.setAdUnitId(DataManager.getInstance().getAdsMobId());
        mAdView.setAdSize(AdSize.SMART_BANNER);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.BOTTOM|Gravity.CENTER;
        params.topMargin = 50;
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)        // All emulators
                .addTestDevice("195E46AB4C8D7D7E7ABA05FE923AE423")
                .addTestDevice("A29E5F35ABB415682047F453C3221695")
                .addTestDevice("738C01DF1094975254FDB275059A8497")
                .build();
        mAdView.loadAd(adRequest);
        container.addView(mAdView,params);

    }
}
