package com.indvzble.freeinternationalcalladvise.manager;

import android.content.Context;
import android.content.res.AssetManager;

import com.google.gson.Gson;
import com.indvzble.freeinternationalcalladvise.model.ItemListModel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;

/**
 * Created by willingdev on 5/17/15.
 */
public class DataManager {

    private   Context context;
    private static DataManager instance;
    private ItemListModel itemListModel;

    public static DataManager getInstance() {
        if(instance==null){
            instance = new DataManager();
        }
        return instance;
    }

    public   void init(Context context){
        this.context = context;
        loadData();
    }

    private void loadData()  {
        AssetManager assetManager = context.getAssets();
        InputStreamReader inputStream = null;
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        try {
            inputStream = new InputStreamReader(assetManager.open("json/core.json"));
            if ( inputStream != null) {
                reader = new BufferedReader(inputStream);
                String mLine = reader.readLine();
                while (mLine != null) {
                    sb.append(mLine);
                    mLine = reader.readLine();
                }
                String json = sb.toString();
                itemListModel = new Gson().fromJson(json, ItemListModel.class);



            }

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(reader!=null){
                try {
                    reader.close();
                } catch (IOException e) {


                }
            }
        }

    }

    public ItemListModel getItemList(){
        if(itemListModel==null){
            ItemListModel itemListModel = new ItemListModel();
            itemListModel.data = new LinkedList<>();
            return itemListModel;
        }
        return itemListModel;
    }

    public String getBannerUrl() {
        return itemListModel.banner;
    }

    public String getBannerLink() {
        return itemListModel.bannerurl;
    }

    public String getAdsMobId() {
        return itemListModel.admobid;
    }
    public String getAdMobIdInterstitial() {
        return itemListModel.admobidinterstitial;
    }
    public String getPollfishId(){
        return itemListModel.pollfish;
    }
}
