package com.indvzble.freeinternationalcalladvise.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indvzble.freeinternationalcalladvise.R;
import com.indvzble.freeinternationalcalladvise.event.ItemSelectedEvent;
import com.indvzble.freeinternationalcalladvise.manager.BusManager;
import com.indvzble.freeinternationalcalladvise.manager.DataManager;
import com.indvzble.freeinternationalcalladvise.model.ItemModel;
import com.indvzble.freeinternationalcalladvise.view.BannerHeaderView;
import com.indvzble.freeinternationalcalladvise.view.CollectionView;
import com.indvzble.freeinternationalcalladvise.view.CollectionViewCallbacks;
import com.indvzble.freeinternationalcalladvise.view.ItemView;
import com.indvzble.freeinternationalcalladvise.view.Rounded;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.LinkedList;
import java.util.List;

public class NewItemListFragment extends Fragment implements CollectionViewCallbacks, LoaderManager.LoaderCallbacks<List<ItemModel>>, View.OnClickListener {

    private static final String IMG_PREFIX = "file:///android_asset/";
    private CollectionView collectionView;
    private List<ItemModel> itemModels = new LinkedList<>();
    private Transformation transformation;

    private int index = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_new_item_list, container, false);
        transformation = new Rounded((int) Rounded.convertDpToPixel(6, getActivity()), Rounded.Corners.TOP);
        collectionView = (CollectionView) rootView.findViewById(R.id.collectionView);
        collectionView.setCollectionAdapter(this);
        getLoaderManager().initLoader(0, null, this);
        return rootView;
    }


    @Override
    public View newCollectionHeaderView(Context context, ViewGroup parent) {
        BannerHeaderView bannerHeaderView = new BannerHeaderView(getActivity().getApplicationContext());
        return bannerHeaderView;
    }

    @Override
    public void bindCollectionHeaderView(Context context, View view, int groupId, String headerLabel) {
        BannerHeaderView bannerHeaderView = (BannerHeaderView) view;
        String bannerUrl = IMG_PREFIX+DataManager.getInstance().getBannerUrl();
        String bannerLink = DataManager.getInstance().getBannerLink();
        bannerHeaderView.imageViewBannerHeader.setTag(bannerLink);
        bannerHeaderView.imageViewBannerHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String bannerLink = (String) v.getTag();
                ItemSelectedEvent event = new ItemSelectedEvent();
                event.author = null;
                event.html = bannerLink;
                event.link =  bannerLink;
                BusManager.getInstance().post(event);
            }
        });

        Picasso.with(context).load(bannerUrl).fit().centerCrop().into(bannerHeaderView.imageViewBannerHeader);
    }

    @Override
    public View newCollectionItemView(Context context, int groupId, ViewGroup parent) {
        return new ItemView(getActivity().getApplicationContext());
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(savedInstanceState!=null){
            index = savedInstanceState.getInt("index",0);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);




        index = collectionView.getFirstVisiblePosition();
        outState.putInt("index",index);


    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

    }

    @Override
    public void bindCollectionItemView(Context context, View view, int groupId, int indexInGroup, int dataIndex, Object tag) {
        if(groupId==1){
            view.setVisibility(View.GONE);
        }else {
            ItemModel itemModel = itemModels.get(dataIndex);
            ItemView itemView = (ItemView) view;
            String imgUrl = IMG_PREFIX+ itemModel.thumb;
            itemView.textViewTitle.setText(itemModel.title);
            itemView.textViewAuthor.setText(itemModel.author);
            Picasso.with(context).load(imgUrl).transform(transformation).fit().centerCrop().into(itemView.imageViewCover);
            itemView.cardView.setOnClickListener(this);
            itemView.cardView.setTag(dataIndex);
        }
    }

    public static NewItemListFragment newInstance() {

        NewItemListFragment fragment = new NewItemListFragment();
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();

        if( itemModels.size()==0){
            getLoaderManager().getLoader(0).forceLoad();

        }
        collectionView.setSelection(index);


    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        return new ItemLoader(getActivity().getApplicationContext());
    }

    @Override
    public void onLoadFinished(Loader<List<ItemModel>> loader, List<ItemModel> data) {
        itemModels = data;
        CollectionView.Inventory inventory = prepareInventory(data);
        collectionView.updateInventory(inventory);
    }

    private CollectionView.Inventory prepareInventory(List<ItemModel> itemList) {

        CollectionView.InventoryGroup headerGroup = new CollectionView.InventoryGroup(1)
                .setDisplayCols(1)
                .setShowHeader(true)
                .setHeaderLabel("");
        headerGroup.addItemWithCustomDataIndex(1);

        CollectionView.InventoryGroup stepGroup = new CollectionView.InventoryGroup(2)
                .setDisplayCols(2)
                .setShowHeader(false);
        if (itemList != null) {
            int size = itemList.size();
            for (int index = 0; index < size; index++) {
                stepGroup.addItemWithCustomDataIndex(index);
            }
        }
        CollectionView.Inventory inventory = new CollectionView.Inventory();
        inventory.addGroup(headerGroup);
        inventory.addGroup(stepGroup);
        return inventory;
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }

    @Override
    public void onClick(View v) {
        int dataIndex = (int) v.getTag();
        ItemModel itemModel = itemModels.get(dataIndex);
        ItemSelectedEvent event = new ItemSelectedEvent();
        event.author = itemModel.author;
        event.html = itemModel.html;
        event.link = itemModel.link;
        BusManager.getInstance().post(event);

    }

    private static class ItemLoader extends AsyncTaskLoader<List<ItemModel>> {

        public ItemLoader(Context context) {
            super(context);
        }

        @Override
        public List<ItemModel> loadInBackground() {

            List<ItemModel> itemModels = DataManager.getInstance().getItemList().data;


            return itemModels;
        }

    }
}
