package com.indvzble.freeinternationalcalladvise;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.indvzble.freeinternationalcalladvise.view.PullToZoomListView;

public class MainActivity extends AppCompatActivity {

	private AdView mAdViewMain;
	private FrameLayout mFrameLayoutMain;
	ItemDataAdapter adapter;
	PullToZoomListView listView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		String getAppname = this.getResources().getString(R.string.app_name);
		getSupportActionBar().setTitle(getAppname);
		setContentView(R.layout.activity_main);

		mFrameLayoutMain = (FrameLayout) findViewById(R.id.wrpAdsMainAc);
		mAdViewMain = new AdView(this);
		mAdViewMain.setAdSize(AdSize.SMART_BANNER);
		mAdViewMain.setAdUnitId(AppConstant.DATA_BANNER_ID);
		AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
		adRequestBuilder.addTestDevice(AppConstant.DATA_TEST_DEVICE);
		mFrameLayoutMain.addView(mAdViewMain);



		mFrameLayoutMain.setVisibility(View.GONE);
		mAdViewMain.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				mFrameLayoutMain.setVisibility(View.VISIBLE);
				super.onAdLoaded();
			}
		});
		mAdViewMain.loadAd(adRequestBuilder.build());

		adapter = new ItemDataAdapter(this, R.layout.item_list_custom_2,
				AppConstant.Data_List);

		listView = (PullToZoomListView) findViewById(R.id.list_view);

		View header = (View) getLayoutInflater().inflate(R.layout.item_header,
				null);
		View footer = (View) getLayoutInflater().inflate(R.layout.item_footer,
				null);
		listView.addHeaderView(header, null, false);
		listView.addFooterView(footer, null, false);
		listView.setEnableZoom(true);
		listView.setParallax(true);
		listView.showHeadView();
		listView.setAdapter(adapter);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (position == 0) {
				} else {
					Log.d("POSITION", "position = " + position);
					position -= listView.getHeaderViewsCount();
					int index = position + 1;
					Log.d("INDEX", "index = " + index);

					Intent intent = new Intent(MainActivity.this,
							LandingActivity.class);
					intent.putExtra("positionT", index + "");
					intent.putExtra("titleExtra", adapter.getItem(position)
							.getTitle());
					intent.putExtra("nameAuth", adapter.getItem(position)
							.getAuth());
					intent.putExtra("linkNameExtra", adapter.getItem(position)
							.getLink());

					startActivity(intent);
				}

			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return false;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return false;
	}

	@Override
	public void onResume() {
		super.onResume();
		if (mAdViewMain != null) {
			mAdViewMain.resume();
		}
	}

	@Override
	public void onPause() {
		if (mAdViewMain != null) {
			mAdViewMain.pause();
		}
		super.onPause();
	}

	@Override
	public void onDestroy() {
		if (mAdViewMain != null) {
			mAdViewMain.destroy();
		}
		super.onDestroy();
	}
}
