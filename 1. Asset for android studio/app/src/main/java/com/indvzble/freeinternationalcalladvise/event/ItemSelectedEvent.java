package com.indvzble.freeinternationalcalladvise.event;

/**
 * Created by willingdev on 5/25/15.
 */
public class ItemSelectedEvent {

    public String html;
    public String link;
    public String author;
}
