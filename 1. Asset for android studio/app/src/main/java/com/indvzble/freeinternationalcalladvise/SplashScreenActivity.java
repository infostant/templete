package com.indvzble.freeinternationalcalladvise;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.squareup.picasso.Picasso;

import java.util.Timer;
import java.util.TimerTask;

public class SplashScreenActivity extends Activity {
	private static final int WAIT_TIME = 6500;
	private InterstitialAd interstitial;
	private Timer waitTimer;
	private boolean interstitialCanceled = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_splash);
		interstitial = new InterstitialAd(this);
		interstitial.setAdUnitId(AppConstant.DATA_INADS_ID);
		interstitial.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {

				if (!interstitialCanceled) {
					waitTimer.cancel();
					interstitial.show();
				}
			}

			@Override
			public void onAdFailedToLoad(int errorCode) {
				startMainActivity();
			}
		});
		interstitial.loadAd(new AdRequest.Builder().addTestDevice(AppConstant.DATA_TEST_DEVICE).build());
		waitTimer = new Timer();
		waitTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				interstitialCanceled = true;
				SplashScreenActivity.this.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						startMainActivity();
					}
				});
			}
		}, WAIT_TIME);
		
		Picasso.with(this).load("file:///android_asset/landingPage/img/banner.jpg")
		.into((ImageView) findViewById(R.id.imgBannerSp));
	}

	@Override
	public void onPause() {
		waitTimer.cancel();
		interstitialCanceled = true;
		super.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
		if (interstitial.isLoaded()) {
			interstitial.show();
		} else if (interstitialCanceled) {
			startMainActivity();
		}
	}

	private void startMainActivity() {
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
		finish();
	}
}
