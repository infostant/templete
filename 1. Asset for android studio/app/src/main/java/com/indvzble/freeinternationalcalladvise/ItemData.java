package com.indvzble.freeinternationalcalladvise;

public class ItemData {
	public String filterColor;
	public boolean haveIcon;
	public String icon;
	public String title;
	public String auth;
	public String link;

	public ItemData() {
		super();
	}

	public ItemData(String filterColor,boolean haveIcon, String title, String auth, String link) {
		super();
		this.filterColor=filterColor;
		this.haveIcon = haveIcon;
		this.title = title;
		this.auth = auth;
		this.link = link;
	}

	public ItemData(String filterColor,boolean haveIcon, String icon, String title, String auth,
			String link) {
		super();
		this.filterColor= filterColor;
		this.haveIcon = haveIcon;
		this.icon = icon;
		this.title = title;
		this.auth = auth;
		this.link = link;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuth() {
		return auth;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

}