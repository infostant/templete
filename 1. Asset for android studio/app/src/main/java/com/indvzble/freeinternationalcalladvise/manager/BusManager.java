package com.indvzble.freeinternationalcalladvise.manager;

import com.squareup.otto.Bus;

/**
 * Created by willingdev on 5/25/15.
 */
public class BusManager {
    private static Bus bus;


    public static void initInstance(){
        getInstance();
    }

    public static Bus getInstance() {
        if(bus==null)
            bus = new Bus();
        return bus;
    }
}
