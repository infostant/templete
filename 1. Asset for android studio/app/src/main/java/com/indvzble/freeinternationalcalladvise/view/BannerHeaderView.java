package com.indvzble.freeinternationalcalladvise.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.indvzble.freeinternationalcalladvise.R;

/**
 * Created by willingdev on 5/21/15.
 */
public class BannerHeaderView extends FrameLayout {

    public ImageView imageViewBannerHeader;
    public BannerHeaderView(Context context) {
        super(context);
        initInstances();
    }

    public BannerHeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initInstances();
    }

    public BannerHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initInstances();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public BannerHeaderView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initInstances();
    }

    private void initInstances(){
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.banner_header, this);
        imageViewBannerHeader = (ImageView) findViewById(R.id.imageViewBannerHeader);
    }
}
