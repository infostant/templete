package com.indvzble.freeinternationalcalladvise;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.indvzble.freeinternationalcalladvise.fragment.NewLandingActivityFragment;
import com.indvzble.freeinternationalcalladvise.manager.AdsManager;
import com.indvzble.freeinternationalcalladvise.manager.DataManager;
import com.pollfish.constants.Position;
import com.pollfish.main.PollFish;


public class NewLandingActivity extends AppCompatActivity {

    private Toolbar mActionBarToolbar;
    private ShareActionProvider mShareActionProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_new_landing);
        if(savedInstanceState==null){
            String html = getIntent().getStringExtra(getString(R.string.html));
            String urlContent = getIntent().getStringExtra(getString(R.string.urlContent));
            String author = getIntent().getStringExtra(getString(R.string.author));
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, NewLandingActivityFragment.newInstance(this,html,urlContent,author))
                    .commit();
        }
        initInstances();


    }

    private void initInstances(){
        setupToolbar();

        FrameLayout mainContainer = (FrameLayout) findViewById(R.id.mainContainer);



       AdsManager.crateAdsView(this, mainContainer);




    }

    private void setupToolbar(){
        if (mActionBarToolbar == null) {
            mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
            if (mActionBarToolbar != null) {
                setSupportActionBar(mActionBarToolbar);
            }
        }
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate menu resource file.
        getMenuInflater().inflate(R.menu.menu_new_landing, menu);

        // Locate MenuItem with ShareActionProvider
        MenuItem item = menu.findItem(R.id.share);

        mShareActionProvider=(ShareActionProvider)MenuItemCompat.getActionProvider(item);


        String urlContent = getIntent().getStringExtra(getString(R.string.urlContent));
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, urlContent);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Check out this site!");
        mShareActionProvider.setShareIntent(intent);

        return true;
    }

    private void initPollfish() {
        PollFish.init(this, DataManager.getInstance().getPollfishId(), Position.MIDDLE_LEFT, 65);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initPollfish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }




}
