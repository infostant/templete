package com.indvzble.freeinternationalcalladvise.model;

/**
 * Created by willingdev on 5/14/15.
 */
public class ItemModel {
    public String title;
    public String thumb;
    public String html;
    public String link;
    public String author;
}
