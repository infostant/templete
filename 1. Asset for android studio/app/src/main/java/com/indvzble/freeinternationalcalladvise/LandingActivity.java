package com.indvzble.freeinternationalcalladvise;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class LandingActivity extends ActionBarActivity {
	
	private AdView mAdViewLanding;
	private FrameLayout mFrameLayout;
	String positionTSt;
	String nameAuth;
	String link_name;
	String title_name;
	TextView content_title;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		setContentView(R.layout.activity_landing);
		
		mFrameLayout = (FrameLayout) findViewById(R.id.wrpLanding);
		mAdViewLanding = new AdView(this);
		mAdViewLanding.setAdSize(AdSize.SMART_BANNER);
		mAdViewLanding.setAdUnitId(AppConstant.DATA_BANNER_ID);
		AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
		adRequestBuilder.addTestDevice(AppConstant.DATA_TEST_DEVICE);
		mFrameLayout.addView(mAdViewLanding);
		mFrameLayout.setVisibility(View.GONE);
		mAdViewLanding.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				mFrameLayout.setVisibility(View.VISIBLE);
				super.onAdLoaded();
			}
		});
		mAdViewLanding.loadAd(adRequestBuilder.build());

		link_name = getIntent().getExtras().getString("linkNameExtra");
		positionTSt = getIntent().getExtras().getString("positionT");
		nameAuth = getIntent().getExtras().getString("nameAuth");
		title_name = getIntent().getExtras().getString("titleExtra");
		
		Log.d("TITLE", title_name+"");
		Log.d("AUTH", nameAuth+"");
		Log.d("LINK", link_name+"");

		getSupportActionBar().setTitle("");
		content_title = (TextView) findViewById(R.id.content_title1);
		content_title.setText(title_name);

		WebView wv = (WebView) findViewById(R.id.webviewContentDetail);
		wv.loadUrl("file:///android_asset/landingPage/appLanding" + positionTSt
				+ "/index.html");
		
		TextView content_author1 = (TextView) findViewById(R.id.author_name1);
		content_author1.setText("By " + nameAuth);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if (mAdViewLanding != null) {
			mAdViewLanding.resume();
		}
	}

	@Override
	public void onPause() {
		if (mAdViewLanding != null) {
			mAdViewLanding.pause();
		}
		super.onPause();
	}

	@Override
	public void onDestroy() {
		if (mAdViewLanding != null) {
			mAdViewLanding.destroy();
		}
		super.onDestroy();
	}

	public void onPressShare1(View view) {
		String address_link = link_name;

		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_TEXT, address_link);
		intent.putExtra(Intent.EXTRA_SUBJECT, "Check out this site!");
		startActivity(Intent.createChooser(intent, "Share"));
	}
}
