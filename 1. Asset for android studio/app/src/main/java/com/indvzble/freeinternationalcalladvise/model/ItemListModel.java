package com.indvzble.freeinternationalcalladvise.model;

import java.util.List;

/**
 * Created by willingdev on 5/19/15.
 */
public class ItemListModel {
    public List<ItemModel> data;
    public String banner;
    public String bannerurl;
    public String admobid;
    public String admobidinterstitial;
    public String pollfish;

}
