package com.indvzble.freeinternationalcalladvise.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.indvzble.freeinternationalcalladvise.R;

/**
 * Created by willingdev on 5/14/15.
 */
public class ItemView extends FrameLayout {
    public CardView cardView;
    public TextView textViewTitle;
    public ImageView imageViewCover;
    public TextView textViewAuthor;


    public ItemView(Context context) {
        super(context);
        initInstances();
    }

    public ItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initInstances();
    }

    public ItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initInstances();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initInstances();
    }
    private void initInstances() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.item_view, this);
        cardView = (CardView) findViewById(R.id.card_view);
        textViewTitle = (TextView) findViewById(R.id.textViewTitle);
        imageViewCover = (ImageView) findViewById(R.id.imageViewCover);
        textViewAuthor = (TextView) findViewById(R.id.textViewAuthor);

    }
}
