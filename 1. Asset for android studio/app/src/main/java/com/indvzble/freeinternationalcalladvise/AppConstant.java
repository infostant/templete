package com.indvzble.freeinternationalcalladvise;

public class AppConstant {
	public static final String DATA_BANNER_ID = "ca-app-pub-5281985935729572/1506826448";
	public static final String DATA_TEST_DEVICE = "8F1D8F1BAEA94CE7087CFA204072527A";
	public static final String DATA_INADS_ID = "ca-app-pub-5281985935729572/2983559641";
	// data for content
	public static final ItemData Data_List[] = new ItemData[] {
		new ItemData("#4d8f3e97", true, "img1.jpg", "Extend android’s battery life", "Carl Parker", "http://www.androidauthority.com/android-battery-saver-tips-tricks-189882/"),
		new ItemData("#4d478fcc", true, "img2.jpg", "Unused device features", "Carl Parker", "http://www.androidauthority.com/android-battery-saver-tips-tricks-189882/"),
		new ItemData("#4d009688", true, "img3.jpg", "Mobile network, Wi-Fi, and network signal", "Carl Parker", "http://www.androidauthority.com/android-battery-saver-tips-tricks-189882/"),
		new ItemData("#4df9ce1e", true, "img4.jpg", "Location services, background data", "Carl Parker", "http://www.androidauthority.com/android-battery-saver-tips-tricks-189882/"),
		new ItemData("#4df0592b", true, "img5.jpg", "“Bad” apps", "Carl Parker", "http://www.androidauthority.com/android-battery-saver-tips-tricks-189882/"),
		new ItemData("#4d4cae4e", true, "img6.jpg", "Power saving mode", "Carl Parker", "http://www.androidauthority.com/android-battery-saver-tips-tricks-189882/"),
		new ItemData("#4d38a4dc", true, "img7.jpg", "Extended batteries and battery care", "Carl Parker", "http://www.androidauthority.com/android-battery-saver-tips-tricks-189882/"),
		new ItemData("#4def4437", true, "img8.jpg", "Conclusion", "Carl Parker", "http://www.androidauthority.com/android-battery-saver-tips-tricks-189882/"), };

}
