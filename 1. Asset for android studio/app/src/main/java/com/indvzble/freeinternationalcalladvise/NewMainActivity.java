package com.indvzble.freeinternationalcalladvise;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.indvzble.freeinternationalcalladvise.event.ItemSelectedEvent;
import com.indvzble.freeinternationalcalladvise.fragment.NewItemListFragment;
import com.indvzble.freeinternationalcalladvise.manager.AdsManager;
import com.indvzble.freeinternationalcalladvise.manager.BusManager;
import com.indvzble.freeinternationalcalladvise.manager.DataManager;
import com.pollfish.constants.Position;
import com.pollfish.main.PollFish;
import com.squareup.otto.Subscribe;


public class NewMainActivity extends AppCompatActivity {

    private Toolbar mActionBarToolbar;
    private InterstitialAd mInterstitialAd;
    private String html;
    private String link;
    private String author;
    Bundle savedInstanceState;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.new_activity_main);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, NewItemListFragment.newInstance())
                            .commit();
        }
        initInstances();

        FrameLayout mainContainer = (FrameLayout) findViewById(R.id.mainContainer);

        AdsManager.crateAdsView(this, mainContainer);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        this.savedInstanceState = outState;
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
    }

    private void initInstances(){
        setupToolbar();

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(DataManager.getInstance().getAdMobIdInterstitial());
        requestNewInterstitial();
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
//                Intent intent = new Intent(NewMainActivity.this, NewLandingActivity.class);
//                intent.putExtra(getString(R.string.html), NewMainActivity.this.html);
//                intent.putExtra(getString(R.string.urlContent), NewMainActivity.this.link);
//                intent.putExtra(getString(R.string.author), NewMainActivity.this.author);
//                startActivity(intent);
            }
        });


    }

    private void initPollfish() {
        if(savedInstanceState==null)
            PollFish.customInit(this, DataManager.getInstance().getPollfishId(), Position.MIDDLE_LEFT, 65);
        else
            PollFish.init(this, DataManager.getInstance().getPollfishId(), Position.MIDDLE_LEFT, 65);

    }
    private void requestNewInterstitial() {

        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("195E46AB4C8D7D7E7ABA05FE923AE423")
                .addTestDevice("A29E5F35ABB415682047F453C3221695")
                .addTestDevice("738C01DF1094975254FDB275059A8497")
                .build();

        mInterstitialAd.loadAd(adRequest);
    }

    private void setupToolbar(){
        if (mActionBarToolbar == null) {
            mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
            if (mActionBarToolbar != null) {
                setSupportActionBar(mActionBarToolbar);
            }
        }
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);

        getSupportActionBar().setTitle(getString(R.string.app_name));

    }

    @Override
    protected void onResume() {
        super.onResume();
        try{
            BusManager.getInstance().register(this);
        }catch (Exception e){

        }


        initPollfish();
        if(savedInstanceState!=null){
            if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();

            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        BusManager.getInstance().unregister(this);
    }

    @Subscribe public void onItemSelected(ItemSelectedEvent event){



            Intent intent = new Intent(this, NewLandingActivity.class);
            intent.putExtra(getString(R.string.html),event.html);
            intent.putExtra(getString(R.string.urlContent),event.link);
            intent.putExtra(getString(R.string.author),event.author);
            startActivity(intent);


    }
}
