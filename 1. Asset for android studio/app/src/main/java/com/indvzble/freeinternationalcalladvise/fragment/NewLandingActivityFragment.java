package com.indvzble.freeinternationalcalladvise.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.indvzble.freeinternationalcalladvise.R;


/**
 * A placeholder fragment containing a simple view.
 */
public class NewLandingActivityFragment extends Fragment {

    private WebView webView;

    public NewLandingActivityFragment() {
    }
    public static NewLandingActivityFragment newInstance(Context context, String html,String urlContent,String author) {
        NewLandingActivityFragment fragment = new NewLandingActivityFragment();
        Bundle args = new Bundle();
        args.putString(context.getString(R.string.html), html);
        args.putString(context.getString(R.string.urlContent),urlContent);
        args.putString(context.getString(R.string.author),author);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView =  inflater.inflate(R.layout.fragment_new_landing, container, false);
        initInstances(rootView);
        return rootView;
    }

    private void initInstances(View rootview){

        TextView textViewAuthor = (TextView) rootview.findViewById(R.id.textViewAuthor);
        String author = getArguments().getString(getString(R.string.author));
        if(author==null){
            textViewAuthor.setVisibility(View.GONE);
        }else
             textViewAuthor.setText(author);


        webView = (WebView) rootview.findViewById(R.id.webviewContentDetail);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

            }

            @Override
            public void onPageFinished(WebView view, String url) {

                super.onPageFinished(view, url);
            }
        });
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
            }
        });
        webView.getSettings().setJavaScriptEnabled(true);


    }

    @Override
    public void onResume() {
        super.onResume();
        String html = getArguments().getString(getString(R.string.html));
        int index = html.indexOf("http");
        if(index==-1)
            webView.loadUrl("file:///android_asset/" + html);
        else
            webView.loadUrl(html);
    }
}
